package org.jakubd.filtering.filter;

//import com.mysema.query.types.Predicate;
import com.querydsl.core.types.Predicate;

public interface PostFilterBuilder {
    Predicate build(PostFilter filter);
}
